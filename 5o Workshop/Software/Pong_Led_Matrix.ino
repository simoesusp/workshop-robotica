//Pong na Matriz de LEDs

// Conecte o Potenciometro (linear de 10K) no pino 5 para controlar a raquete

// Conecte um auto-falante (buzzer) no pino 3 para o som do joguino

// Se defender 10 bolas, voce vence o jogo, que reinicia automaticamente!!

#include <LedControl.h>
int DIN = 10;
int CS =  9;
int CLK = 8;

LedControl myMatrix=LedControl(DIN,CLK,CS,0);
byte smile[8]=   {0x3C,0x42,0xA5,0x81,0xA5,0x99,0x42,0x3C};
byte sad[8]=   {0x3C,0x42,0xA5,0x81,0x99,0xA5,0x42,0x3C};

int coluna = 1, linha = random(8)+1;               //decide em que ponto a bola deve começar
int direcaoX = 1, direcaoY = 1;                   //certifica-se de que ela vai primeiro da esquerda para a direita
int raquete = 5, raqueteVal;                     //pino e valor do potenciômetro
int velocidade = 300;
int contador = 0, mult = 10, pontos = 0;

void setup()
{

    myMatrix.shutdown(0, false);                 //habilita o display
    myMatrix.setIntensity(0, 8);                 //define o brilho como médio
    myMatrix.clearDisplay(0);                    //limpa o display
    randomSeed(analogRead(0));                   //cria uma semente genuinamente aleatória
  
  
}

void loop()
{

    raqueteVal = analogRead(raquete);
    raqueteVal = map(raqueteVal, 200, 1024, 1, 6);
    coluna += direcaoX;
    linha += direcaoY;

    if(coluna == 6 && direcaoX == 1 && (raqueteVal == linha || raqueteVal+1 == linha || raqueteVal+2 == linha)) {
      pontos ++;
      direcaoX = -1;
      tone(3, 200, 400); //  tone(pino, frequência, duração)
    }

    if(coluna == 0 && direcaoX == -1) {
      tone(3, 600, 300); //  tone(pino, frequência, duração)
      direcaoX = 1;
    }

    if(linha == 7 && direcaoY == 1 ) {
      tone(3, 700, 300); //  tone(pino, frequência, duração)
      direcaoY = -1;
    }

    if(linha == 0 && direcaoY == -1) {
      tone(3, 700, 300); //  tone(pino, frequência, duração)
      direcaoY = 1;
    }

    if(coluna == 7) oops();

    if(pontos > 9) ganhou();

//|||||||||||||||||||||||||||||||||||||||||||||||
    myMatrix.clearDisplay(0);                    //limpa tela para próxima quadro de animação
    myMatrix.setLed(0, coluna, linha, HIGH);
    myMatrix.setLed(0, 7, raqueteVal, HIGH);
    myMatrix.setLed(0, 7, raqueteVal+1, HIGH);
    myMatrix.setLed(0, 7, raqueteVal+2, HIGH);

    if(!(contador % mult)) {velocidade -= 5; mult*=mult;}
    delay(velocidade);

    contador++;
  
  
} //end loop


void oops()
{
  for(int x=0; x<3; x++)
  {
    myMatrix.clearDisplay(0);
    delay(250);
       for(int y=0; y<8; y++)
       {
          myMatrix.setRow(0, y, 255); 
        
       }
tone(3, 400, 300); //  tone(pino, frequência, duração)
       delay(50);
    
  } //end for

  printByte(sad);    
  delay(3000);

  contador = 0;  //reinicia todos os valores
  velocidade = 300;
  coluna = 1;
  linha = random(8)+1;
  pontos = 0;
  
  
} //end oops

void ganhou()
{
  for(int x=0; x<3; x++)
  {
    myMatrix.clearDisplay(0);
    delay(250);
       for(int y=0; y<8; y++)
       {
          myMatrix.setRow(0, y, 255); 
        
       }
tone(3, 800, 300); //  tone(pino, frequência, duração)
       delay(50);
    
  } //end for

  printByte(smile);    
  delay(5000);

  contador = 0;  //reinicia todos os valores
  velocidade = 300;
  coluna = 1;
  linha = random(8)+1;
  pontos = 0;
  
  
} //end ganhou

void printByte(byte character [])
{
  int i = 0;
  for(i=0;i<8;i++)
  {
    myMatrix.setRow(0,i,character[i]);
  }
}

// C++ code
//

const int led_RED_Pin = 11;      // o pino que o led esta' tem que ter suporte a PWM... --> O pin 13 nao tem!!
const int led_GREEN_Pin = 10;      // o pino que o led esta' tem que ter suporte a PWM... --> O pin 13 nao tem!!
const int led_BLUE_Pin = 9;      // o pino que o led esta' tem que ter suporte a PWM... --> O pin 13 nao tem!!


int brightness_RED, brightness_GREEN, brightness_BLUE;

String buffer;

void setup() {
  // initialize the serial communication:
  Serial.begin(9600);
  // initialize the ledPin as an output:
  pinMode(led_RED_Pin, OUTPUT);
  pinMode(led_GREEN_Pin, OUTPUT);
  pinMode(led_BLUE_Pin, OUTPUT);
}

void loop() {

  while(!Serial.available()) {}
      brightness_RED = Serial.parseInt(); // Essa funcao so' pega de -32000 ate' +32000 pois o int do Arduino e' de 2 bytes
      analogWrite(led_RED_Pin, brightness_RED);
      Serial.print("brightness_RED = ");
    Serial.println(brightness_RED);
  
  while(!Serial.available()) {}
      brightness_GREEN = Serial.parseInt(); // Essa funcao so' pega de -32000 ate' +32000 pois o int do Arduino e' de 2 bytes
      analogWrite(led_GREEN_Pin, brightness_GREEN);
      Serial.print("brightness_GREEN = ");
    Serial.println(brightness_GREEN);
  
  while(!Serial.available()) {}
      brightness_BLUE = Serial.parseInt(); // Essa funcao so' pega de -32000 ate' +32000 pois o int do Arduino e' de 2 bytes
      analogWrite(led_BLUE_Pin, brightness_BLUE);
      Serial.print("brightness_BLUE = ");
      Serial.println(brightness_BLUE);
  
}

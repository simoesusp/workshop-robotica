# 5o Workshop de Robótica na Educação - Arduino Básico Sofisticado

- Vídeo do 5o Workshop - a ser disponibilisado

- Neste Workshop serão discutidas as funcionalidades básicas do microcontrolador  Arduino de maneira mais aprofundada. Essa abordagem tem o intuito de explicar mais detalhadamente as funções mais simples que permitem a conexão de sensores e atuadores no Arduino. Esse workshop é voltado para um público iniciante na ferramenta de projeto Arduino e irá possibilitar ao público discutir variações e tirar dúvidas sobre os robôs e sistemas de automação propostos nós Workshops anteriores.

Algumas Páginas importantes que iremos usar neste Workshop:

- Documentação de Referência da Linguagem Arduino - https://www.arduino.cc/reference/pt/

- Arduino Uno Tutorial: Pinout - https://diyi0t.com/arduino-uno-tutorial/

- ITP Physical Computing - https://itp.nyu.edu/physcomp/lessons/

- Para reprogramar o Processador ATMEGA ATmega328p do Arduino UNO - Programador Gravador Avr Usbasp Usbisp Atmel Impressora 3d - [Link](https://produto.mercadolivre.com.br/MLB-1897201309-programador-gravador-avr-usbasp-usbisp-atmel-impressora-3d-_JM?matt_tool=87716990&matt_word=&matt_source=google&matt_campaign_id=12413740998&matt_ad_group_id=119070072438&matt_match_type=&matt_network=g&matt_device=c&matt_creative=500702333978&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=427086074&matt_product_id=MLB1897201309&matt_product_partition_id=337120033364&matt_target_id=pla-337120033364&gclid=Cj0KCQjwjo2JBhCRARIsAFG667UPFKS1LjVL5qTvs9QvhPAhXuGfdrxb-CNQuTPtAQIJs0ZnQa5SPC8aAo3QEALw_wcB)


## Semaforo de Pedestres
- O softare do Semaforo pode ser visto no projeto do TinkerCad ([Link](https://www.tinkercad.com/things/eQNMlrT0ET0)) ou pode ser programado diretamente da interface do Arduino com o programa Semaforo.ino , disponível na pasta Software. 

 ## LED Dimmer

 - Esse é um exemplo de como mudar o brilho de um Led enviando o valor pela prta serial do PC. O software esta disponivel na pasta Software ou no projeto do TinkerCad ([Link](https://www.tinkercad.com/things/lpMOaux02mw)).

<div align="center">
 <img src="./Imagens/LED_Dimmer.png" width="300">
</div>


 ## LED RGB Controle pelaSerial

 - Esse é um exemplo de como mudar o brilho de um Led enviando o valor pela prta serial do PC. O software esta disponivel na pasta Software ou no projeto do TinkerCad ([Link](https://www.tinkercad.com/things/55LwuEUpnB6)).

<div align="center">
 <img src="./Imagens/LED_RGB_Controle_pela_Serial.png" width="300">
</div>

## Matriz de Leds

- Esse é um exemplo de como desenhar em uma matriz de LEDs de 8x8 com o MAX 7219. O software esta disponivel na pasta Software, pois o projeto do TinkerCad ([Link](https://www.tinkercad.com/things/gEOWUA85Il7)) não contém essea matriz de LEDs.

<div align="center">
 <img src="./Imagens/Screenshot_from_2021-08-09_13-58-16.png" width="300">
 <img src="./Imagens/Smile.jpeg" width="300">
</div>

- Pong na Matriz de LEDs
  -
  - O software esta disponivel na pasta Software: Pong_Led_Matrix.ino

<div align="center">
 <img src="./Imagens/Pong.jpeg" width="300">
 <img src="./Imagens/Sad.jpeg" width="300">
</div>


# Orçamento do 5o Workshop

- Os preços abaixo estão sem o frete, em média o frete para cada item varia entre R$10,00 e R$20,00 - Recomenda-se entrar em contato com o vendedor para tentar comprar a maioria dos itens da mesma loja, para reduzir o frete.

- As ferramentas necessárias são bastante comuns, como tesoura, cola quente, chave de fenda, alicate. Também pode ser necessário uma furadeira e uma serra de metal, bem como um ferro de soldar equipamentos eletrônicos.
- Para aprofundar os conhecimentos dos alunos em eletrônica, é recomendável também a utilisação de um Multimetro Digital Portátil (Preço R$32,99) [Link](https://produto.mercadolivre.com.br/MLB-1565053094-multimetro-digital-portatil-profissional-multiteste-com-cabo-_JM#position=1&type=item&tracking_id=3ce6cbd7-dbb4-42a3-8561-01dbf16d5658) 


### Tabela com os componentes do circuito

|   Nome do componente   | Preço (sem frete) | Link Mercadolivre  |
| ---------------------- | ----------------- | ----------------- |
|  Módulo Matriz De Led 8x8 Com Max7219 Led 3mm       | R$21,90           | [Link](https://produto.mercadolivre.com.br/MLB-808274739-modulo-matriz-de-led-8x8-com-max7219-led-3mm-_JM#position=10&search_layout=stack&type=item&tracking_id=9bf56a4c-326a-4f21-9fbf-5c84a8951cfa) |
|  20 Peças - Led 5mm Rgb 4 Terminais      | R$47,96           | [Link](https://produto.mercadolivre.com.br/MLB-1215335964-20-pecas-led-5mm-rgb-4-terminais-arduino-pic-promoco-_JM?searchVariation=35580150122#searchVariation=35580150122&position=6&search_layout=stack&type=item&tracking_id=d68c33e0-6994-4bf6-9db4-c8371ac8fbd6) |



# 4o Workshop de Robótica na Educação - Automação de Irrigação

- Vídeo do 4o Workshop - https://youtu.be/GVvc5KWfuqg

- Serão apresentados tecnologias robóticas para automatizar hortas e canteiros de flores em ambientes escolares, casas e até mesmo pequenas varandas de apartamentos. Mais especificamente, o sistema robótico proposto deverá controlar o acionamento de válvulas de irrigação e até mesmo a direção de aspersores robóticos para regar o jardim somente nas áreas que mais necessitam. Serão utilizados sensores de umidade do solo, sensores de iluminação e intensidade do vento. Com essa informação, a inteligência artificial do sistema pode calcular a quantidade de água para a irrigação, com o objetivo de economizar esse recurso cada vez mais precioso.

- O softare do Robô pode ser visto no projeto do TinkerCad ([Link](https://www.tinkercad.com/things/8grdmSXXZS3)) ou pode ser programado diretamente da interface do Arduino com o programa sensor_Controle_Irrigacao.ino , disponível nesta pasta. 

- Link para vídeo do arpersor robótico em operação...  [Link](https://youtu.be/sXgkFoXjWTg)

## Versão com Válvula de Máquina de Lavar Roupas

- Essa versão usa uma válvula de máquina de lavar para controlar um volume maior de água. Pŕopria para hortas grandes no terreno da Escola. Podem ser adicionadas mais válvulas para irrigar de maneira diferente as regiões da horta.

<div align="center">
 <img src="./Imagens/WhatsApp_Image_2021-07-12_at_12.55.14_PM__1_.jpeg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-07-12_at_12.55.15_PM.jpeg" width="300">
</div>

- Link para vídeo do arpersor robótico em operação com válvula de máquina de lavar...  [Link](https://youtu.be/aAv8efy2JsI)

## Versão com ESP32 para ler a previsão do tempo na internet

- Essa versão usa um segundo microcontrolador, o ESP2, para se conectar na internet e ler a previsão do tempo para o dia seguinte e assim fornecer mais informações para a IA omar a decisão de quanto tempo irrigar a horta.
- O novo código do Arduino (Controle_Irrigacao_Previsao_Tempo.ino e prevTempo.h) estão disponíveis nesta pasta e o código do ESP32 (EspSensor.ino e prevTempo.h) estão disponíveis na pasta SoftwareESP32.
- O cricuito com as conexões com o ESP32 pode ser visto na figura abaixo. 

<div align="center">
 <img src="./Imagens/Controle_Irrigacao_Workshop_2.png" width="300">
</div>

# Orçamento do Projeto de Irrigação

- Os preços abaixo estão sem o frete, em média o frete para cada item varia entre R$10,00 e R$20,00 - Recomenda-se entrar em contato com o vendedor para tentar comprar a maioria dos itens da mesma loja, para reduzir o frete.

- As ferramentas necessárias são bastante comuns, como tesoura, cola quente, chave de fenda, alicate. Também pode ser necessário uma furadeira e uma serra de metal, bem como um ferro de soldar equipamentos eletrônicos.
- Para aprofundar os conhecimentos dos alunos em eletrônica, é recomendável também a utilisação de um Multimetro Digital Portátil (Preço R$32,99) [Link](https://produto.mercadolivre.com.br/MLB-1565053094-multimetro-digital-portatil-profissional-multiteste-com-cabo-_JM#position=1&type=item&tracking_id=3ce6cbd7-dbb4-42a3-8561-01dbf16d5658) 


### Tabela com os componentes do circuito

|   Nome do componente   | Preço (sem frete) | Link Mercadolivre  |
| ---------------------- | ----------------- | ----------------- |
|  Micro Servo 9g Sg90       | R$30,90           | [Link](https://produto.mercadolivre.com.br/MLB-1520017308-micro-servo-9g-sg90-para-projeto-arduino-aeromodelo-original-_JM#position=3&search_layout=stack&type=item&tracking_id=45963d02-9c4f-4ea3-ad78-4ae06f7b40d9) |
|  Sensor De Umidade Do Solo  | R$15,90           | [Link](https://produto.mercadolivre.com.br/MLB-1659220537-sensor-de-umidade-do-solo-higrmetro-para-arduino-_JM#position=1&search_layout=stack&type=item&tracking_id=79ebe65a-b579-4cb7-880d-d2dd921325b8) |
|  Sensor Sonar       | R$18,96           | [Link](https://produto.mercadolivre.com.br/MLB-795588540-modulo-sensor-sonar-distancia-pra-arduino-nodemcu-esp8266-_JM#position=15&type=item&tracking_id=41e80598-9eb1-428f-b68c-457e7ddbcdeb) |
|  Modulo Rele 1 Canal       | R$16,96           | [Link](https://produto.mercadolivre.com.br/MLB-1364870549-modulo-rele-1-canal-led-indicador-para-arduino-pi-pic-5v10a-_JM#position=4&search_layout=grid&type=item&tracking_id=890b05eb-dcf3-4c69-bd09-42b5c146e286) |
|  Sensor De Temperatura Ds18b20 Prova Dágua       | R$21,96           | [Link](https://produto.mercadolivre.com.br/MLB-1799390165-sensor-de-temperatura-ds18b20-prova-dagua-p-projeto-arduino-_JM#position=3&search_layout=stack&type=item&tracking_id=6d657e0b-5ccc-436b-a009-32b3d7c74ee7) |
|  Kit 10 Ldr 5mm Fotoresistor       | R$9,96           | [Link](https://produto.mercadolivre.com.br/MLB-1799390165-sensor-de-temperatura-ds18b20-prova-dagua-p-projeto-arduino-_JM#position=3&search_layout=stack&type=item&tracking_id=6d657e0b-5ccc-436b-a009-32b3d7c74ee7) |
|  Bombinha Eletro Bomba Gasolina Agua 12v       | R$27,96           | [Link](https://produto.mercadolivre.com.br/MLB-1673527057-bombinha-eletro-bomba-gasolina-agua-12v-2-saidas-universal-_JM) |
|  Válvula Solenoide Simples Entrada De Água 127v       | R$27,96           | [Link](https://produto.mercadolivre.com.br/MLB-1550068875-valvula-solenoide-simples-entrada-de-agua-127v-com-suporte-_JM#position=13&search_layout=stack&type=item&tracking_id=20c614aa-3cde-441c-afcc-63b900973ad8) |
|  Esp32 Wroom Devkit 38 Pinos       | R$45,96           | [Link](https://produto.mercadolivre.com.br/MLB-1742094266-esp32-wroom-devkit-38-pinos-_JM#position=9&search_layout=grid&type=item&tracking_id=9cd5656a-32cb-4885-b038-eebd697e646a) |


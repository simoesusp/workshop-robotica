#include <WiFi.h>
#include <HTTPClient.h>
#include <string>

class PrevTempo {
  public:
   PrevTempo(std::string city): _city(city){}

   void atualiza() {
      if ((WiFi.status() == WL_CONNECTED)) { //Check the current connection status
 
    HTTPClient http;
 
    http.begin("http://wttr.in/"+String(_city.c_str())); //Specify the URL
    int httpCode = http.GET();                                        //Make the request
 
    if (httpCode > 0) { //Check for the returning code
 
        String payloadString = http.getString();
        std::string payload(payloadString.c_str());
        // Get temperature
        {
          int indexTempBef = payload.find("°C")-15;
          String tempStr = getValue(payload, indexTempBef);
          temperatura = tempStr.toInt();
        }
        // Get wind
        {
          int indexWindBef = payload.find("km/h")-15;
          String windStr = getValue(payload, indexWindBef);
          vento = windStr.toInt();
        }
        // Get rain mm
        {
          int indexRainBef = payload.find("mm")-15;
          String rainmmStr = getValue(payload, indexRainBef);
          chuva = rainmmStr.toFloat();
        }

        // Get rain mm for each day
        {
          int currIndex = payload.find("mm")+1;// Ignore current rain mm
          // For each day
          for(int i=0;i<3;i++) {
            // For each Morning/Noon/Evening/Night
            float maxRain = 0;
            for(int j=0;j<4;j++) {
              int indexRain = payload.find("mm", currIndex);
              currIndex = indexRain+1;
              // TODO Maybe it will broke when rain mm >=10 
              String rainmmStr = getValue(payload, indexRain-6, " ", "m");
              float rain = rainmmStr.toFloat();
              if(rain>maxRain)
                maxRain = rain;
            }
            chuvaMax[i] = maxRain;
          }
        }

      }
 
    else {
      Serial.println("Error on HTTP request");
    }
 
    http.end(); //Free the resources
  }
   }
   

   int temperatura = 0;
   int vento = 0;
   float chuva = 0;
   float chuvaMax[3];

   private:
    String getValue(std::string payload, int approxIndex, std::string minn=">", std::string maxx="<") {
      int indexInit = payload.find(minn, approxIndex)+1;
      int indexEnd = payload.find(maxx, approxIndex)-1;
      int numSize = indexEnd-indexInit+1;
      std::string numericalStd = payload.substr(indexInit, numSize).c_str();
      String dataString(numericalStd.c_str());
    
      return dataString;
    }

    std::string _city;
};

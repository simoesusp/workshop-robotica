#include <WiFi.h>
#include <HTTPClient.h>
#include <string>
#include "prevTempo.h"
#include <SoftwareSerial.h>
#include <WebServer.h>
SoftwareSerial serialEsp(18, 19); //(RX, TX)

#define LED_BUILTIN 2
WebServer server(80);
 
const char* ssid = "NET_2G956978";
const char* password =  "95956978";

PrevTempo prevTempo("Sao Carlos");

void configura(){}
void inicio(){}
void para(){}
void handleNotFound(){}
 
void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  delay(4000);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");

   server.on("/configura", HTTP_GET, configura); 
   server.on("/inicio", HTTP_GET, inicio); 
   server.on("/para", HTTP_GET, para); 

   server.onNotFound(handleNotFound);

    server.on("/ligaled", []() {
      server.send(200, "text/plain", "ligou");
      digitalWrite(LED_BUILTIN, HIGH);
    });

  server.on("/desligaled", []() {
      server.send(200, "text/plain", "apagou");
      digitalWrite(LED_BUILTIN, LOW);
    });

  server.begin();
  Serial.println("HTTP server started");
 
  serialEsp.begin(9600);
}

 
void loop() {
  
  prevTempo.atualiza();
  char temp = prevTempo.temperatura;
  char wind = prevTempo.vento;
  char rain = int(prevTempo.chuvaMax[1]*10);

  
  serialEsp.print("t");
  serialEsp.print(temp);
  serialEsp.print("w");
  serialEsp.print(wind);
  serialEsp.print("r");
  serialEsp.print(rain);
  Serial.print("t");
  Serial.print(prevTempo.temperatura);
  Serial.print("w");
  Serial.print(prevTempo.vento);
  Serial.print("r");
  Serial.print(rain);

  for(int i=0;i<10;i++) {
    server.handleClient();
    delay(100);
  }
/*
  if(serialEsp.available())
  {
    Serial.print((char)serialEsp.read());
  }
  if(Serial.available())
  {
    serialEsp.print((char)Serial.read());
  }*/
}

#include <NeoSWSerial.h>

NeoSWSerial serialEsp(11, 2);

class PrevTempo {
  public:
   PrevTempo(){
      serialEsp.begin(9600);
    }

   void atualiza() {
      while(serialEsp.available()) {
        buffer += (char)serialEsp.read();
        if(buffer.length()==2) {
          if(buffer[0]=='t') {
            temperatura = (int)buffer[1];
            buffer = "";
          }else if(buffer[0]=='w') {
            vento = (int)buffer[1];
            buffer = "";
          }else if(buffer[0]=='r') {
            chuvaAmanha = float(int(buffer[1]))/10;
            buffer = "";
          }else {
            buffer = "";
          }
        }else if(buffer.length()>2)
          buffer = "";
      }
   }
   
   int temperatura = 100;
   int vento = 0;
   float chuvaAmanha = 0;
   
   private:
      String buffer;
};

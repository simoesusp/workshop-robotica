/*
 Sensor de linha e Sensor RGB
*/

#include "Adafruit_TCS34725.h"  // Biblioteca para acessar o sensor RGB
                                // Não esqueça de instalar essa biblioteca em Ferramentas -> Gerenciar Bibliotecas -> Topico = Adafruit_TCS34725
                                // Não precisa reinicializar!

// Para mais de m sensor RGB instalar por aqui: https://github.com/Fire7/Adafruit_TCS34725_SoftI2C
                                
// instância do sensor RGB
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);

// instância para variáveis do sensor
uint16_t r, g, b, c, colorTemp, lux;
  
int LED1, LED2, LED3, LED4;
int Vermelho, Verde, Azul;

// A funçaõ setup roda apenas uma vez quando liga o Arduino
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Inicializa o pino LED_BUILTIN as an output.
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);
  Serial.begin(9600); // Configura velocidade da porta serial
                      // Não esqueça de conferir se o terminal está em 9600 também!

  delay(500);                       // espera por 1/2 secondos

  if (tcs.begin()) { // se o sensor foi detectado, entao
    Serial.println("Sensor ok       "); // mensagem de sensor encontrado
  } else { // se sensor não encontrado
    Serial.println("Nao ha sensor"); // mensagem de nao sensor encontrado
    while (1);
  }
} // Final Setup

// A função Loop irá rodar para sempre
void loop() {

   // leitura de dados de tons de cores e luminosidade
   tcs.getRawData(&r, &g, &b, &c);
   
   // cálculo dos níveis de cores
   colorTemp = tcs.calculateColorTemperature(r, g, b);

   // cálculo de nível de luminosidade
   lux = tcs.calculateLux(r, g, b);

  Serial.print("r= ");
  Serial.print(r);
  Serial.print(" g= ");
  Serial.print(g);
  Serial.print(" b= ");
  Serial.print(b);
  Serial.print(" c= ");
  Serial.print(c);

  // se claridade for menor que 5000, então
  if(c < 5000){
    if (r>b && r>g){ // tom vermelho maiore que azul e verde
      Vermelho = 1;
      Verde = 0;
      Azul = 0;
      Serial.println(" Cor= Vermelho");
      }
    else if(g>r && g>b){// tom verde maiore que azul e vermelho
      Vermelho = 0;
      Verde = 1;
      Azul = 0;
      Serial.println(" Cor= Verde");
      }
    else if(b>r && b>g){ // tom azul maiore que vermelho e verde
      Vermelho = 0;
      Verde = 0;
      Azul = 1;
      Serial.println(" Cor= Azul");
    }
    else{
      Vermelho = 0;
      Verde = 0;
      Azul = 0;
      Serial.println(" Cor= INDEFINIDA");
    }
   }
  else Serial.println(" Muita Luz !");
   

  // Sensores de Linha
  LED1 = digitalRead(2);
  LED2 = digitalRead(3);
  LED3 = digitalRead(4);
  LED4 = digitalRead(5);
  Serial.print("LED1= ");
  Serial.print(LED1);
  Serial.print(" LED2= ");
  Serial.print(LED2);
  Serial.print(" LED3= ");
  Serial.print(LED3);
  Serial.print(" LED4= ");
  Serial.println(LED4);

  digitalWrite(LED_BUILTIN, HIGH);   // Liaga o LED do Arduino
  delay(500);                       // espera por 1/2 secondos
  digitalWrite(LED_BUILTIN, LOW);    // Apaga o LED do Arduino
  delay(500);                       // espera por 1/2 secondos
}

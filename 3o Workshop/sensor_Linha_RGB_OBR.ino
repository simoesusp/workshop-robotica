/*
 Sensor de linha e Sensor RGB
*/
#define VEL_ALTA 135
#define VEL_MEDIA 120
#define VEL_BAIXA 65
#define VEL_DIF 18  // Foi preciso calibrar a velocidade dos dois motores pois estava um pouco diferente

#include "Adafruit_TCS34725.h"  // Biblioteca para acessar o sensor RGB
                                // Não esqueça de instalar essa biblioteca em Ferramentas -> Gerenciar Bibliotecas -> Topico = Adafruit_TCS34725
                                // Não precisa reinicializar!

// Para mais de um sensor RGB instalar da forma que explica aqui: https://github.com/Fire7/Adafruit_TCS34725_SoftI2C
                                
// instância do sensor RGB
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);

// instância para variáveis do sensor
uint16_t r, g, b, c, colorTemp, lux;
  
int LED1, LED2, LED3, LED4;
int Vermelho, Verde, Azul;

// Função que le os sonares
long readUltrasonicDistance(int triggerPin, int echoPin)
{
  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);
  // Sets the trigger pin to HIGH state for 10 microseconds
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  // Reads the echo pin, and returns the sound wave travel time in microseconds
  return pulseIn(echoPin, HIGH);
}

// A funçaõ setup roda apenas uma vez quando liga o Arduino
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Inicializa o pino LED_BUILTIN as an output.
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);

  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  
  Serial.begin(9600); // Configura velocidade da porta serial
                      // Não esqueça de conferir se o terminal está em 9600 também!

  delay(500);                       // espera por 1/2 segundos

  if (tcs.begin()) { // se o sensor foi detectado, entao
    Serial.println("Sensor ok       "); // mensagem de sensor encontrado
  } else { // se sensor não encontrado
    Serial.println("Nao ha sensor"); // mensagem de nao sensor encontrado
    while (1);
  }
} // Final do Setup

// A função Loop irá rodar para sempre
void loop() {

   // leitura de dados de tons de cores e luminosidade
   tcs.getRawData(&r, &g, &b, &c);
   
   // cálculo dos níveis de cores
   colorTemp = tcs.calculateColorTemperature(r, g, b);

   // cálculo de nível de luminosidade
   lux = tcs.calculateLux(r, g, b);

  Serial.print("r= ");
  Serial.print(r);
  Serial.print(" g= ");
  Serial.print(g);
  Serial.print(" b= ");
  Serial.print(b);
  Serial.print(" c= ");
  Serial.print(c);

  // se claridade for menor que 5000, então
  if(c < 5000){
    if (r>b && r>g){ // tom vermelho maiore que azul e verde
      Vermelho = 1;
      Verde = 0;
      Azul = 0;
      Serial.println(" Cor= Vermelho");
      }
    else if(g>r && g>b){// tom verde maiore que azul e vermelho
      Vermelho = 0;
      Verde = 1;
      Azul = 0;
      Serial.println(" Cor= Verde");
      }
    else if(b>r && b>g){ // tom azul maiore que vermelho e verde
      Vermelho = 0;
      Verde = 0;
      Azul = 1;
      Serial.println(" Cor= Azul");
    }
    else{
      Vermelho = 0;
      Verde = 0;
      Azul = 0;
      Serial.println(" Cor= INDEFINIDA");
    }
   }
  else Serial.println(" Muita Luz !");
   

  // Sensores de Linha
  LED1 = digitalRead(2);
  LED2 = digitalRead(3);
  LED3 = digitalRead(4);
  LED4 = digitalRead(5);
  Serial.print("LED1= ");
  Serial.print(LED1);
  Serial.print(" LED2= ");
  Serial.print(LED2);
  Serial.print(" LED3= ");
  Serial.print(LED3);
  Serial.print(" LED4= ");
  Serial.println(LED4);

/*
  if (0.01723 * readUltrasonicDistance(13, 11) < 50 && 0.01723 * readUltrasonicDistance(12, 10) < 50) {  // (triggerPin, echoPin)
    analogWrite(6, 0);
    analogWrite(9, 0);
  } else 
  */
  if (Vermelho == 1) {
      analogWrite(6, 0);
      analogWrite(9, 0);
  } else if (Verde == 1) {
      analogWrite(6, 0);
      analogWrite(9, VEL_ALTA);
      delay(1000);  // virar por 1 segundo
  } else if (digitalRead(2) == 1) {
      analogWrite(6, VEL_BAIXA);
      analogWrite(9, VEL_ALTA);
  } else if (digitalRead(5) == 1) {
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_BAIXA);
  } else if (digitalRead(3) == 1 && digitalRead(4) == 1){
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_ALTA);
  } else if (digitalRead(3) == 1) {
      analogWrite(6, VEL_MEDIA+VEL_DIF);
      analogWrite(9, VEL_ALTA);
  } else if (digitalRead(4) == 1) {
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_MEDIA);
  } else {
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_ALTA);
  }
    
  delay(10);                       // espera por 10 milisegundos
}

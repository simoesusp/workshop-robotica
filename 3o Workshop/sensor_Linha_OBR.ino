/*
 Sensor de linha
*/
#define VEL_ALTA 135
#define VEL_MEDIA 120
#define VEL_BAIXA 65
#define VEL_DIF 18  // Foi preciso calibrar a velocidade dos dois motores pois estava um pouco diferente

int LED1, LED2, LED3, LED4;

// A função setup roda apenas uma vez quando liga o Arduino
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Inicializa o pino LED_BUILTIN as an output.
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);

  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  
  Serial.begin(9600); // Configura velocidade da porta serial
                      // Não esqueça de conferir se o terminal está em 9600 também!
} // Final Setup

// A função Loop irá rodar para sempre
void loop() {

/*   
  // Imprime os Sensores de Linha para testar
  LED1 = digitalRead(2);
  LED2 = digitalRead(3);
  LED3 = digitalRead(4);
  LED4 = digitalRead(5);
  Serial.print("LED1= ");
  Serial.print(LED1);
  Serial.print(" LED2= ");
  Serial.print(LED2);
  Serial.print(" LED3= ");
  Serial.print(LED3);
  Serial.print(" LED4= ");
  Serial.println(LED4);
*/

  // Lê os sensores de Linha e dirige o robô
  if (digitalRead(2) == 1) {
      analogWrite(6, VEL_BAIXA);
      analogWrite(9, VEL_ALTA);
  } else if (digitalRead(5) == 1) {
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_BAIXA);
  } else if (digitalRead(3) == 1 && digitalRead(4) == 1){
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_ALTA);
  } else if (digitalRead(3) == 1) {
      analogWrite(6, VEL_MEDIA+VEL_DIF);
      analogWrite(9, VEL_ALTA);
  } else if (digitalRead(4) == 1) {
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_MEDIA);
  } else {
      analogWrite(6, VEL_ALTA+VEL_DIF);
      analogWrite(9, VEL_ALTA);
  }
    
  delay(10);                       // espera por 10 milisegundos
}

# 3o Workshop de Robótica na Educação - Projeto Robô Seguidor de Linha

- Este Workshop visa montar um robô adaptado do primeiro Workshop para poder competir na Olimpíada Brasileira de Robótica. Inicialmente iremos adicionar um Sensor de Linha Infravermelho de 4 Vias ([Link](https://produto.mercadolivre.com.br/MLB-1234954162-sensor-infravermelho-linha-obstaculo-4-vias-canais-arduino-_JM)). 
Para depois conectar um Sensor de Cor RGB Tcs34725 ([Link](https://produto.mercadolivre.com.br/MLB-977106643-sensor-de-cor-rgb-tcs34725-com-filtro-ir-arduino-_JM)).

- Link para vídeo do robô seguindo a linha...  [Link](https://drive.google.com/file/d/157995YB7Gcc2d2n7FdaBK5LmNjO6VQH4/view?usp=sharing)

- Link para vídeo do robô seguindo a linha bem de pertinho...  [Link](https://drive.google.com/file/d/157Ou99ukeYLw8TTjkbXPkTHbMnL4yfGm/view?usp=sharing)

- O softare do Robô pode ser visto no projeto do TinkerCad ([Link](https://www.tinkercad.com/things/g1NVjNb61Fp)) ou pode ser programado diretamente da interface do Arduino com o programa sensor_Linha_RGB_OBR.ino , disponíve nesta pasta. Para usar somente o seguidor de Linha infravermelho, usar o programa sensor_Linha_OBR.ino. E para testar a leitura dos valores dos sensores para calibração, usar o programa: Teste_sensor_Linha.ino.
- O circuito do robô pode ser compreendido na figura, iterconectando o Arduino na bateria, ponte H, Sensor de linha infravermelho e sensor de cor RGB. Na figura pode-se ver o robô sobre uma linha preta feita com fita isolante em um piso claro.

<div align="center">
 <img src="./Imagens/Sensor_RGB_Conection_Tinker_cad.jpg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.44.48_PM__2_.jpeg" width="300">
</div>

- A própria criança pode brincar definindo a pista, com circuitos bem desafiadores. Tomar cuidado para que a fita fique totalmente colada no chão, sem rugas.

<div align="center">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.44.48_PM__7_.jpeg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.44.48_PM__5_.jpeg" width="300">
</div>

- Como o circuito do sensor seguidor de Linha infravermelho é bastando grande, ele foi acomodado dentro do robô e colado com cola quente ao lado da bateria na parte traseira. Deve-se prestar atenção para que os potenciômetros da regulagem da sensibilidade fiquem bem acessíveis.

<div align="center">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.44.48_PM__3_.jpeg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.44.48_PM__4_.jpeg" width="300">
</div>

- Vista de cima do robô, mostrando as conexões dos sensores com o Arduino. O professor deve intervir somente quando a "bagunça" dos fios apresentar possíveis curtos ou mal contato, ou quando alguns fios possam entrar em contato com as rodas do robô.

<div align="center">
 <img src="./Imagens/WhatsApp_Image_2021-06-14_at_11.08.54_AM.jpeg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-06-14_at_11.08.55_AM.jpeg" width="300">
</div>

- Nessa última imágem, foi instalado uma baqueta para que o robo possa tocar música batendo em copos dágua ao longo do percurso!

<div align="center">
 <img src="./Imagens/WhatsApp_Image_2021-06-14_at_11.08.55_AM__1_.jpeg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-06-14_at_11.08.55_AM__2_.jpeg" width="300">
</div>

- Link para vídeo do robô tocando o Dó Ré Mi Fá...  [Link](https://drive.google.com/file/d/15PrH8x4T0uzHKe5aDq_vyzd3L2gRmNM2/view?usp=sharing)

- Essa tentativa parou no Si... [Link](https://drive.google.com/file/d/15UGrypiKlulifRBxZ9avhSRnZlX1KTc1/view?usp=sharing)


## Sensor de Cor RGB

- Para utilizar um sensor de Cor RGB Tcs34725 ([Link](https://produto.mercadolivre.com.br/MLB-977106643-sensor-de-cor-rgb-tcs34725-com-filtro-ir-arduino-_JM)) deve-se conectálo à porta I2C do Arduino, nos pinos Analógicas A4 e A5. Para acessá-lo, é preciso instalar uma biblioteca de software que contenha a informação para se comunicar com o sensor. Essa biblioteca é a (#include "Adafruit_TCS34725.h").

- Não esqueça de instalar essa biblioteca em Ferramentas -> Gerenciar Bibliotecas -> Topico = Adafruit_TCS34725. Não precisa reinicializar a IDE do Arduino depois!

<div align="center">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.54.55_PM__1_.jpeg" width="300">
 <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.54.55_PM__2_.jpeg" width="300">
</div>

- A figura ensina a conectar o sensor RGB no Arduino. A alimentção de 5V e o GND foram pegos a partir dos pequenos pinos centrais, pois o resto do circuito do robô não deixou nenhum dos pinos normais de VCC e GND disponíveis!

<div align="center">
  <img src="./Imagens/Sensor_RGB_Conection.jpg" width="300">
  <img src="./Imagens/WhatsApp_Image_2021-06-13_at_10.54.55_PM.jpeg" width="300">
</div>

- Para um segundo sensor de Cor, deve-se usar um emulador de I2C em software, como explicado em ([Link]( https://github.com/Fire7/Adafruit_TCS34725_SoftI2C)). Isso é necessário porque o sensor Tcs34725 possui um endereço fixo que não pode ser alterado, então, se forem conectados mais de um sensor Tcs34725 na porta I2C do Arduino, ele ão vai identificá-los adequadamente. Então, pode-se emular uma segunda porta I2C com qualquer outrs pinos digitais e conectar nela o segundo sensor Tcs34725.


# Orçamento do Robô do 2o e 3o Workshops
- Para completar o robô do 1o Workshop e capacitá-lo a seguir linhas e resolver os desafios das pistas da Olimpíada de Robótica, é necessário adquirir mais um módulo de Sonar, um módulo com 4 sensores Infra-vermelhos e dois módulos com sensores de cor RGB.

- Os preços abaixo estão sem o frete, em média o frete para cada item varia entre R$10,00 e R$20,00 - Recomenda-se entrar em contato com o vendedor para tentar comprar a maioria dos itens da mesma loja, para reduzir o frete.

- As ferramentas necessárias são bastante comuns, como tesoura, cola quente, chave de fenda, alicate. Também pode ser necessário uma furadeira e uma serra de metal, bem como um ferro de soldar equipamentos eletrônicos.
- Para aprofundar os conhecimentos dos alunos em eletrônica, é recomendável também a utilisação de um Multimetro Digital Portátil (Preço R$32,99) [Link](https://produto.mercadolivre.com.br/MLB-1565053094-multimetro-digital-portatil-profissional-multiteste-com-cabo-_JM#position=1&type=item&tracking_id=3ce6cbd7-dbb4-42a3-8561-01dbf16d5658) 


### Móduloas Seguidor de linha para completar o Robô do 1o Workshop
|   Nome do componente   | Preço (sem frete) | Link Mercadolivre  |
| ---------------------- | ----------------- | ----------------- |
|  Sensor Infravermelho Linha Obstáculo 4 Vias Canais       | R$23,90           | [Link](https://produto.mercadolivre.com.br/MLB-1234954162-sensor-infravermelho-linha-obstaculo-4-vias-canais-arduino-_JM) |
|  2x Sensor De Cor Rgb Tcs34725 Com Filtro Ir  | R$39,90           | [Link](https://produto.mercadolivre.com.br/MLB-977106643-sensor-de-cor-rgb-tcs34725-com-filtro-ir-arduino-_JM) |
|  Sensor Sonar       | R$18,96           | [Link](https://produto.mercadolivre.com.br/MLB-795588540-modulo-sensor-sonar-distancia-pra-arduino-nodemcu-esp8266-_JM#position=15&type=item&tracking_id=41e80598-9eb1-428f-b68c-457e7ddbcdeb) |

# Orçamentos do Robô do 1o Workshop
Os orçamentos estão separados para cada robô, mas componentes como cabo/pilhas podem ser comprados só uma vez e utilizados nos dois robôs.
Os preços abaixo estão sem o frete, em média o frete para cada item varia entre R$10,00 e R$20,00 - Recomenda-se entrar em contato com o vendedor para tentar comprar a maioria dos itens da mesma loja, para reduzir o frete.
Além destes componentes, serão necessários outros materiais que podem ser reutilisados, como CDs/DVs para fabricar as rodas, e uma placa de aço, alumínio, madeira, plastico ou papelão para fabricar o chassis.
As ferramentas necessárias são bastante comuns, como tesoura, cola quente, chave de fenda, alicate. Também pode ser necessário uma furadeira e uma serra de metal, bem como um ferro de soldar equipamentos eletrônicos.
Para aprofundar os conhecimentos dos alunos em eletrônica, é recomendável também a utilisação de um Multimetro Digital Portátil (Preço R$32,99) [Link](https://produto.mercadolivre.com.br/MLB-1565053094-multimetro-digital-portatil-profissional-multiteste-com-cabo-_JM#position=1&type=item&tracking_id=3ce6cbd7-dbb4-42a3-8561-01dbf16d5658) 

Também é possível utilizar este [motor alternativo](https://microred.com.br/page18.html) ao invés do amarelo.

### Robôzinho Pequeno Workshop
|   Nome do componente   | Preço (sem frete) | Link Mercadolivre  |
| ---------------------- | ----------------- | ----------------- |
|  2x Socket Pilha       | R$19,90           | [Link](https://produto.mercadolivre.com.br/MLB-1521394688-suporte-porta-4-pilhas-aa-com-plug-p4-para-arduino-esp8266-_JM#position=5&type=item&tracking_id=b91c2986-2a0d-4a6b-bebb-cf1b5bc3e79d) |
|  1x 12 Unidades Pilha *  | R$21,90           | [Link](https://produto.mercadolivre.com.br/MLB-1511526264-kit-12-pilhas-panasonic-comum-pequena-aa-cartela-c4-unid-_JM#position=2&type=item&tracking_id=06afe6c3-05b0-4b43-a4b6-42d870448dde) |
|  1x Driver Motor L298n | R$20,49           | [Link](https://produto.mercadolivre.com.br/MLB-1347048749-driver-motor-ponte-h-dupla-l298n-arduino-pic-automaco-_JM#position=6&type=item&tracking_id=f8eca4d3-78b6-44f3-bc54-23112f72446d) |
|  1x Arduino UNO        | R$42,90           | [Link](https://produto.mercadolivre.com.br/MLB-1740670920-compativel-erduino-placa-uno-r3-atmega328p-cabo-usb-_JM#position=2&type=item&tracking_id=da968aba-46fe-4fa8-be46-37588ebfa485) |
|  2x Sensor Sonar       | R$18,96           | [Link](https://produto.mercadolivre.com.br/MLB-795588540-modulo-sensor-sonar-distancia-pra-arduino-nodemcu-esp8266-_JM#position=15&type=item&tracking_id=41e80598-9eb1-428f-b68c-457e7ddbcdeb) |
|  2x Motor com Pneu     | R$14,90           | [Link](https://produto.mercadolivre.com.br/MLB-975586495-roda-pneu-motor-dc-6v-robotica-arduinoleves-defeitos-_JM#position=27&type=item&tracking_id=57133b40-6bde-460b-8cd5-396729eeb870) |
|  1x Cabo macho-femea   | R$15,90           | [Link](https://produto.mercadolivre.com.br/MLB-985669692-cabo-wire-jumper-20cm-40-fios-fmea-macho-protoboard-arduino-_JM#position=1&type=item&tracking_id=420b2897-eaaf-44cb-82ff-a155240b856e) |
|  1x Cabo macho-macho   | R$15,90           | [Link](https://produto.mercadolivre.com.br/MLB-1199393339-cabo-wire-jumper-20cm-40-fios-macho-macho-protoboard-arduino-_JM#position=3&type=item&tracking_id=bc01dfb0-cc08-49bd-8fc4-56f97a1c7aa0) |

* O robô necessita apenas 8 pilhas AA 

### Grilo Eletrônico

|   Nome do componente   | Preço (sem frete) | Link para compra  |
| ---------------------- | ----------------- | ----------------- |
|  1x Socket Pilha       | R$19,90           | [Link](https://produto.mercadolivre.com.br/MLB-1521394688-suporte-porta-4-pilhas-aa-com-plug-p4-para-arduino-esp8266-_JM#position=5&type=item&tracking_id=b91c2986-2a0d-4a6b-bebb-cf1b5bc3e79d) |
|  1x 12 Unidades Pilha **  | R$21,90           | [Link](https://produto.mercadolivre.com.br/MLB-1511526264-kit-12-pilhas-panasonic-comum-pequena-aa-cartela-c4-unid-_JM#position=2&type=item&tracking_id=06afe6c3-05b0-4b43-a4b6-42d870448dde) |
|  1x Arduino UNO        | R$42,90           | [Link](https://produto.mercadolivre.com.br/MLB-1740670920-compativel-erduino-placa-uno-r3-atmega328p-cabo-usb-_JM#position=2&type=item&tracking_id=da968aba-46fe-4fa8-be46-37588ebfa485) |
|  1x Buzzer       | R$18,96           | [Link](https://produto.mercadolivre.com.br/MLB-1316165544-modulo-buzzer-mini-ativo-buzina-beep-para-arduino-pic-arm-_JM#position=2&type=item&tracking_id=92c53e7a-d235-4f83-85f6-04e163821ac0) |
|  1x Cabo macho-femea   | R$15,90           | [Link](https://produto.mercadolivre.com.br/MLB-985669692-cabo-wire-jumper-20cm-40-fios-fmea-macho-protoboard-arduino-_JM#position=1&type=item&tracking_id=420b2897-eaaf-44cb-82ff-a155240b856e) |
|  1x Cabo macho-macho   | R$15,90           | [Link](https://produto.mercadolivre.com.br/MLB-1199393339-cabo-wire-jumper-20cm-40-fios-macho-macho-protoboard-arduino-_JM#position=3&type=item&tracking_id=bc01dfb0-cc08-49bd-8fc4-56f97a1c7aa0) |

** O nosso Grilo eletrônico necessita apenas 4 pilhas AA

### Outros componentes

|   Nome do componente   | Preço (sem frete) | Link para compra  |
| ---------------------- | ----------------- | ----------------- |
|   1x protoboard   | R$11,78 | [Link](https://produto.mercadolivre.com.br/MLB-1388477292-protoboard-breadboard-400-pontos-furos-pinos-_JM) |
|   1x Bateria de Lipo  3.7v | R$30,00 | [Link](https://produto.mercadolivre.com.br/MLB-1434648646-bateria-li-po-37v-150mah-recarregavel-27x20x4mm-_JM#position=15&type=item&tracking_id=ec7aa005-3dad-45ef-9a96-0aad9777e2c7) |


# Workshop Robotica na Educação

## Contato
- Discord do nosso Workshop: https://discord.gg/EhGZ8Qzpzt
- O email do Grupo Principia-Robôs na Escola: principia@icmc.usp.br
- Site do Principia-Robôs na Escola: https://principia.icmc.usp.br/

# Competição 2o Workshop -> Premio ARDUINO UNO !!!!
- Não perca! Participe do nosso segundo Workshop, baixe o simulador sBotics e compita nas pistas do grande desafio! O vencedor que completar a pista com a maior pontuação, no menor tempo possível, ganhará um Arduino UNO !!!
- Importe nossa Arena com a Pista Complicada - https://gitlab.com/simoesusp/workshop-robotica/-/blob/master/2oWorkshop/ArenaPistaComplicada.arena
- Se quiser umas dicas de como programar o robô, aqui está um exemplo de código usando o Editor de Bloccos que consegue completar o percurso... muitomais ou menos... quase sempre... Você pode melhorá-lo ou fazer o seu do Zero!!  -  https://gitlab.com/simoesusp/workshop-robotica/-/blob/master/2oWorkshop/PistaComplicada.sboticsB
- Se não conseguirvencer essa pista... Não desista! Pegue outras arenas para treinar (todas com softwre exemplo): https://gitlab.com/simoesusp/workshop-robotica/-/tree/master/2oWorkshop

- O prazo para enviar um Screan Shot para o email principia@icmc.usp.br com o final da pista,mostrando sua pontuação e o tempo para completar a pista é até o dia 7 de junho de 2021. No próximo 3o Worshop de Robótica do dia 14 de junho de 2021, os 10 melhores  times irão competir ao vivo e os 3 primeiros lugares irão ganhar um Arduino UNO como prêmio!! 

# 1o Workshop de Robótica na Educação - 15/03/2021

### Vídeo ensinando a montar o Robô:
- Parte 1: https://www.youtube.com/watch?v=NO65XY2hsPk

- Parte 2: https://www.youtube.com/watch?v=1NJt_PJfG_g 

- Reportagem SBT: https://www.youtube.com/watch?v=GvfKDbubgmQ

[Slides](https://drive.google.com/drive/folders/1jc7Qu8VszNamWopHLWn2ACnm9MSyLu4b?usp=sharing)<br><br>

### Robô Pequeno 

[Link do Thinkercad](https://www.tinkercad.com/things/0GASpmNucxc)<br><br>

<img src="https://drive.google.com/uc?export=dowload&id=1-o9Elaq7xda-aFqMnVMV9hicWfYbcSQk" width=300>
<img src="https://drive.google.com/uc?export=dowload&id=1h7YbpioH1D9riG5Qfzj6omXvIv-92G2B" width=300>
<img src="https://drive.google.com/uc?export=dowload&id=1K1TqcOdGr_nq1TalHBwJj1ZTykDPPwoi" width=300><br>

### Grilo Eletrônico

[Link do Thinkercad](https://www.tinkercad.com/things/kjTXRIwTxCJ)<br><br>

<img src="https://drive.google.com/uc?export=dowload&id=1i9J17_LaLlQ5832L23I1ibUl4efPLnJb" width=300><br>

# 2o Workshop de Robótica na Educação - 17/05/2021

### Olimpíada Brasileira de Robótica

### Vídeo completo do 2o Workshop: https://www.youtube.com/watch?v=1E0ZfNgJTMc

### Discord do nosso Workshop: https://discord.gg/EhGZ8Qzpzt
- Use o discord para colocar suas perguntas e duvidas sobre os Workshops deRobótica na Educação

- Site da OBR e informações sobre as competições - http://www.obr.org.br/modalidade-pratica/como-participar-modalidade-pratica/

- O Simulador sBotics - Download e informações - https://sbotics.weduc.natalnet.br/#download
  - LINUX: Como instalar o arquivo sBotics.AppImage - https://sempreupdate.com.br/como-executar-arquivo-appimage-no-linux/
  - Para funcionar o Editor de Blocos, você tem que encontrar o programa e selecionar a permissão para "executar como programa".

<div align="center">
 <img src="./2oWorkshop/Screenshot_from_2021-05-17_11-38-44.png" height="300">
</div>
  

- Modalidade Prática Virtual - Manual de Regras e Instruções - Versão 1.1 – Abril de 2021 - http://www.obr.org.br/manuais/OBR2021_MP_ManualRegrasSIMEstadual.pdf

- Tutorial Oficial sBotics - https://sbotics.github.io/tutorial/content/index.html?lang=pt_BR

- Tutorial Primeiros passos - https://sbotics.github.io/tutorial/content/guide/getting_started.html?lang=pt_BR
  - Instalando o programa
  - Configurando o iniciador
  - Gerando nova arena
  - Escolhendo robô
  - Qual linguagem de programação escolher?

- Seu primeiro seguidor de linha em rEduc - https://sbotics.github.io/tutorial/content/guide/line_follower.html?lang=pt_BR

- Manual das Funções de comando do Robô - https://sbotics.weduc.natalnet.br/functions/reduc_ptbr

- LIVE - OBR 2021: O que é e como usar o Simulador sBotics - https://www.youtube.com/watch?v=Sq4ug2A2dhQ

- Tutorial JeyLAB Robótica - OBR 2020 Virtual - Simulador sBotics - https://www.youtube.com/playlist?list=PL8Mq5-TcVPBwMNTg4lbCi1irWMP2Fal3s

<div align="center">
 <img src="./2oWorkshop/Screenshot_from_2021-05-17_10-32-00.png" height="300">
</div>  



# 3o Workshop de Robótica na Educação - 14/06/2021

### Olimpíada Brasileira de Robótica - Construção do Robô

### Vídeo completo do 3o Workshop: https://www.youtube.com/watch?v=2fooUV0WKWQ

- Para saber mais, entre na pasta específica do 3o Workshop!

<div align="center">
 <img src="./3o%20Workshop/Imagens/Sensor_RGB_Conection_Tinker_cad.jpg" width="300">
 <img src="./3o%20Workshop/Imagens/WhatsApp_Image_2021-06-13_at_10.44.48_PM__2_.jpeg" width="300">
</div>

[Link do Thinkercad](https://www.tinkercad.com/things/g1NVjNb61Fp)<br><br>


# 4o Workshop de Robótica na Educação - 12/07/2021

### Controle de Irrigação de Horta na Escola

### Vídeo completo do 4o Workshop: - https://youtu.be/GVvc5KWfuqg

- Para saber mais, entre na pasta específica do 4o Workshop!

<div align="center">
 <img src="./4o%20Workshop/Imagens/Controle_Irrigacao_Workshop.png" width="300">
 <img src="./4o%20Workshop/Imagens/image.png" width="300">
</div>

<div align="center">
 <img src="./4o%20Workshop/Imagens/WhatsApp_Image_2021-07-11_at_12.54.36_PM.jpeg" width="300">
 <img src="./4o%20Workshop/Imagens/WhatsApp_Image_2021-07-11_at_12.54.37_PM__1_.jpeg" width="300">
</div>

### Video do aspersor Robótico funcionando: https://youtu.be/vEdmypo5sGs

[Link do Thinkercad](https://www.tinkercad.com/things/8grdmSXXZS3)<br><br>


# 5o Workshop de Robótica na Educação - 09/08/2021

### Arduino Básico Sofisticado

### Vídeo completo do 5o Workshop: - a ser disponibilisado

- Para saber mais, entre na pasta específica do 5o Workshop!

<div align="center">
 <img src="./5o%20Workshop/Imagens/Semaforo.png" width="300">
 <img src="./5o%20Workshop/Imagens/Semaforo2.png" width="300">
</div>

[Link do Thinkercad](https://www.tinkercad.com/things/eQNMlrT0ET0)<br><br>

# Orçamento do Robô do 2o e 3o Workshops
- Para completar o robô do 1o Workshop e capacitá-lo a seguir linhas e resolver os desafios das pistas da Olimpíada de Robótica, é necessário adquirir mais um módulo de Sonar, um módulo com 4 sensores Infra-vermelhos e dois módulos com sensores de cor RGB.

- Os preços abaixo estão sem o frete, em média o frete para cada item varia entre R$10,00 e R$20,00 - Recomenda-se entrar em contato com o vendedor para tentar comprar a maioria dos itens da mesma loja, para reduzir o frete.

- As ferramentas necessárias são bastante comuns, como tesoura, cola quente, chave de fenda, alicate. Também pode ser necessário uma furadeira e uma serra de metal, bem como um ferro de soldar equipamentos eletrônicos.
- Para aprofundar os conhecimentos dos alunos em eletrônica, é recomendável também a utilisação de um Multimetro Digital Portátil (Preço R$32,99) [Link](https://produto.mercadolivre.com.br/MLB-1565053094-multimetro-digital-portatil-profissional-multiteste-com-cabo-_JM#position=1&type=item&tracking_id=3ce6cbd7-dbb4-42a3-8561-01dbf16d5658) 


### Móduloas Seguidor de linha para completar o Robô do 1o Workshop
|   Nome do componente   | Preço (sem frete) | Link Mercadolivre  |
| ---------------------- | ----------------- | ----------------- |
|  Sensor Infravermelho Linha Obstáculo 4 Vias Canais       | R$23,90           | [Link](https://produto.mercadolivre.com.br/MLB-1234954162-sensor-infravermelho-linha-obstaculo-4-vias-canais-arduino-_JM) |
|  2x Sensor De Cor Rgb Tcs34725 Com Filtro Ir  | R$39,90           | [Link](https://produto.mercadolivre.com.br/MLB-977106643-sensor-de-cor-rgb-tcs34725-com-filtro-ir-arduino-_JM) |
|  Sensor Sonar       | R$18,96           | [Link](https://produto.mercadolivre.com.br/MLB-795588540-modulo-sensor-sonar-distancia-pra-arduino-nodemcu-esp8266-_JM#position=15&type=item&tracking_id=41e80598-9eb1-428f-b68c-457e7ddbcdeb) |
